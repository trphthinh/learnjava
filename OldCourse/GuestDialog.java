import javax.swing.JOptionPane; 

public class GuestDialog {
  public static void main( String [] args) {
    String guestname = JOptionPane.showInputDialog("What is your name, please"); 
    String message = String.format("Hello %s", guestname); 
    JOptionPane.showMessageDialog(null, message); 
  }
}
