import java.security.SecureRandom; 

public class StudentPoll
{
  public static void main(String[] args) {
    // student response array (more typically, input at runtime)
    int nPoll = 6000000; 
    int [] response = new int[nPoll]; 

    // initialize poll result
    SecureRandom randomNumbers = new SecureRandom(); 

    for (int iter = 0; iter < nPoll; iter++)  {
      response[iter] = randomNumbers.nextInt(6);
    }


    int [] frequency = new int[6]; 

    for (int iter = 0; iter < response.length; iter++) {
      try
      {
        ++frequency[response[iter]];
      }
      catch (ArrayIndexOutOfBoundsException e) {
        System.out.printf(" responses[%d] = %d\n\n", iter, response[iter]);
      }
    }

    System.out.printf("%s%10s\n", "Rating", "Frequency"); 
    
    // output each array element's value
    for (int rating = 0; rating < frequency.length; rating++) {
      System.out.printf("%6d%10d\n", rating, frequency[rating]); 
    }
 
  }
}
