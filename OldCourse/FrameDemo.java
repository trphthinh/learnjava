import javax.swing.JFrame; 
import javax.swing.JLabel; 
import java.awt.Container; 
import java.awt.BorderLayout; 

public class FrameDemo {
  public static void main ( String [] args ) {
    
    //1. Create the frame
    JFrame frame = new JFrame("FrameDemo"); 
    
    //2. Optional: What happens when the frame closes?
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 

    //3. Create components and put them in the frame. 
    JLabel emptyLabel = new JLabel("Text-Only Label"); 
    frame.getContentPane().add(emptyLabel, BorderLayout.CENTER); 

    //4. Size the frame. 
    frame.pack(); 

    //5. Show it. 
    frame.setVisible(true); 

  }
}




