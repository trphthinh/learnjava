import javax.swing.JOptionPane; 

public class InputDialog{
  public static void main( String [] args) {
    String input = JOptionPane.showInputDialog("What is input?"); 
    String message = String.format("Your input is %s", input); 
    JOptionPane.showMessageDialog(null, message); 
  }
}
